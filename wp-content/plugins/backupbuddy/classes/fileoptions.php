<?php
/*
$backup_options = new pb_backupbuddy_fileoptions( $filename );
$backup_options->options = array();
*/

class pb_backupbuddy_fileoptions {
	
	public $options;
	private $_file;
	
	// Reads and creates file lock. If file does not exist, creates it. Places options into this class's $options.
	function __construct( $file, $ignore_lock = false ) {
		
		$this->_file = $file;
		
		if ( ! file_exists( dirname( $file ) ) ) { // Directory exist?
			pb_backupbuddy::anti_directory_browsing( dirname( $file ), $die_on_fail = false, $deny_all = true );
		}
		
		if ( ! file_exists( $file ) ) { // File exist?
			// Create file.
		}
		
		if ( $this->is_locked() ) {
			pb_backupbuddy::status( 'error', 'Unable to read fileoptions file `' . $file . '` as it is currently locked.' );
			return false;
		}
		
		// Get options and decode into usable format.
		$options = file_get_contents( $file );
		if ( false === $options ) {
			...
			return false;
		}
		if ( false === ( $options = base64_decode( $options ) ) ) {
			...
			return false;
		}
		if ( false === ( $options = maybe_unserialize( $options ) ) ) {
			...
			return false;
		}
		
		$this->_options = $options;
		return true;
		
	}
	
	// Saves & removes file lock.
	function __destruct() {
		$this->save( $remove_lock = true );
	}
	
	// Save now without removing lock.
	function save( $remove_lock = false ) {
		
		$options = base64_encode( serialize( $options ) );
		
	} // End save().
	
	function lock() {
	}
	
	function unlock() {
	}
	
	function is_locked() {
	}
	
	/*
	public function get_stable_options( $id ) {
		
		$lock_file = pb_backupbuddy::$options['temp_directory'] . 'stable_options_' . $id . '.lock';
		if ( file_exists( $lock_file ) ) { // file locked.
			$unlock_time = file( $lock_file );
			$unlock_time = $unlock_time[1]; // Skip first line.
			echo 'unlock: ' . $unlock_time;
			echo 'now: ' .time();
			if ( time() >= $unlock_time ) { // Unlock.
				unlink( $lock_file );
			} else { // LOCKED still.
				return false;
			}
		}
		
	} // End get_locked_options().
	
	
	
	// seconds from current time to auto unlock.
	public function set_stable_options( $id, $value, $unlock_seconds ) {
		
		$lock_file = pb_backupbuddy::$options['temp_directory'] . 'stable_options_' . $id . '.lock';
		if ( file_exists( $lock_file ) ) { // file locked.
			$unlock_time = file( $lock_file );
			$unlock_time = $unlock_time[1]; // Skip first line.
			if ( time() >= $unlock_time ) { // Unlock.
				unlink( $lock_file );
			} else { // LOCKED still.
				return false;
			}
		}
		
		file_put_contents( pb_backupbuddy::$options['temp_directory'] . 'stable_options_' . $id, '<?php die(); ?>' . "\n" . $value );
		file_put_contents( pb_backupbuddy::$options['temp_directory'] . 'stable_options_' . $id . '.lock', '<?php die(); ?>' . "\n" . ( time()+$unlock_seconds ) );
		
	} // End set_locked_options().
	*/
	
} // End class pb_backupbuddy_fileoptions.