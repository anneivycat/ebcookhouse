<?php
/**
 *  Plugin name: Everbody's Menu
 *  Description: Custom Post type for Everybody's menu items
 *  Author: IvyCat
 *  Version: 1.0
 */

if (!function_exists ('add_action')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}

class EBCookHouseCPT{
    
    public function __construct(){
        add_action( 'init', array( $this, 'post_type_init' ) );
        add_action( 'admin_init' , array( &$this, 'metabox_init' ) );
        add_action( 'save_post' , array( &$this, 'save_cpt_metadata' ) );
    }
    
    public function post_type_init(){
        $labels = array(
            'name'=>__( "Menu Items" ),
            'singular_name'=>__( "Menu Item" ),
            'add_new'=>__( 'New Menu Item' ),
            'add_new_item'=>__( 'Add New Menu Item' ),
            'edit_item'=>__( 'Edit Menu Item' ),
            'new_item'=>__( 'Add New Menu Item' ),
            'view_item'=>__( 'View Menu Item' ),
            'search_items'=>__( 'Search Menu Items' )
        );
        
        // Change what you want it to support here.
        $supports = array('title','editor','thumbnail');
        
        $args = array(
            'label'=> __( "Menu Items" ),
            'labels'=>$labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true, 
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'menu_position' => 4,
            'supports' => $supports
        );

        register_post_type( 'menu-item' ,$args);
        
        $tax = array("hierarchical" => true,
            "label" => "Menu Categories",
            "singular_label" =>"Menu Category",
            "rewrite" => array('hierarchical'=>true));
        register_taxonomy( 'menu-category', 'menu-item', $tax );
      
    }
    
    public function metabox_init(){
        add_meta_box(
            'Menu Iteminfo-meta',
            'Menu Item Data',
            array( &$this, 'info_metabox' ),
            'menu-item', 'normal', 'core'
        );
    }
    
    public function info_metabox(){
        global $post;
        require_once 'lib/views/item_info_view.php';
    }
    
    public function save_cpt_metadata(){
        if( defined( 'DOING_AJAX') ) return;
        if( !is_array( $_POST['menu_item'] ) ) return;
        global $post;
        foreach( $_POST['menu_item'] as $item_name => $item_value ){
            update_post_meta( $post->ID, 'menu_item_' . $item_name, $item_value );
        }
        //echo '<pre>' . print_r( $_POST, true ) . '</pre>';
    }
}

add_action( 'plugins_loaded', 'EBCookHouseCPT_init' );

function EBCookHouseCPT_init(){
    new EBCookHouseCPT();
}
