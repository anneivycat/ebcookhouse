<fieldset>
    <ul>
        <li>
            <label for="item-price">Price<label>
            <input id="item-price" type="text" name="menu_item[price]" value="<?php echo get_post_meta( $post->ID, 'menu_item_price', true ); ?>"/>
        </li>
        <li>
            <label for="item-serving-size">Serving Size</label>
            <input id="item-serving-size" type="text" name="menu_item[serving_size]" value="<?php echo get_post_meta( $post->ID, 'menu_item_serving_size', true ); ?>"/>
        </li>
        <li>
            <label for="item-servings">Servings</label>
            <input id="item-servings" type="text" name="menu_item[servings]" value="<?php echo get_post_meta( $post->ID, 'menu_item_servings', true ); ?>"/>
        </li>
        <li>
            <label for="item-calories">calories</label>
            <input id="item-calories" type="text" name="menu_item[calories]" value="<?php echo get_post_meta( $post->ID, 'menu_item_calories', true ); ?>"/>
        </li>
        <li>
            <label for="item-unsatfat">unsat. fat</label>
            <input id="item-unsatfat" type="text" name="menu_item[unsat_fat]" value="<?php echo get_post_meta( $post->ID, 'menu_item_unsat_fat', true ); ?>"/>
        </li>
        <li>
            <label for="item-satfat">sat. fat</label>
            <input id="item-satfat" type="text" name="menu_item[sat_fat]" value="<?php echo get_post_meta( $post->ID, 'menu_item_sat_fat', true ); ?>"/>
        </li>
        <li>
            <label for="item-carbs">carbs</label>
            <input id="item-carbs" type="text" name="menu_item[carbs]" value="<?php echo get_post_meta( $post->ID, 'menu_item_carbs', true ); ?>"/>
        </li>
        <li>
            <label for="item-protein">protein</label>
            <input id="item-protein" type="text" name="menu_item[protein]" value="<?php echo get_post_meta( $post->ID, 'menu_item_protein', true ); ?>"/>
        </li>
    </ul>
</fieldset>
