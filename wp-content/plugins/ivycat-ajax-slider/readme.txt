=== IvyCat AJAX Image Slider ===
Contributors: dgilfoy, ivycat, sewmyheadon
Donate link: http://www.ivycat.com/contribute/
Tags: shortcode, ajax, slider, featured image, image switcher, image fader
Requires at least: 3.0
Tested up to: 3.4.2
Version 1.1.2
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Simple image slider that uses AJAX to load and switch images.

== Description ==

This plugin is a lightweight template/framework for developers to easily add a powerful AJAX image slider to a page.

= Features =

* Use simple shortcodes to add a slider to a page.
* Create multiple slider groups.
* Supports one slider per page.
* The first slide loads when the page loads and the rest are pulled in via AJAX, speeding up initial page load.
* Uses a custom post type for the slides making it easy for you, or customers, to add new slides and groups.
* Uses the featured image of the slide's post.
* You create the slide order.
* Change slide speed, fadeIn/fadeOut durations, remove base styles, and even remove next/previous arrows and slide button icons

Plugin depends upon your theme's style and _does not_ contain native styles.  

This is a minimal plugin, function over form.  If you would like to extend it, or would like us to extend it in later versions, feel free to post ideas in this [plugin's support forum](http://wordpress.org/support/plugin/ivycat-ajax-slider), or [contact us](http://www.ivycat.com/contact/).

Shortcodes:

	* `[ic_do_slider]` - run the default slider
	* `[ic_do_slider group="slug_or_id_of_group_term"]` - adds slider with the specified group ( ID of the group term, or it's slug )
	* `[ic_do_slider quantity="5"]` - changes the default slide quantity from 3 to 5
	* `[ic_do_slider thumb_size="thumbnail"]` - changes the default slide featured image size to thumbnail (default is medium - thumbnail, medium, large, full or custom - if supported )
	* `[ic_do_slider fadeIn=600 fadeOut=700 speed=7000]` - duration and speed of slider (in miliseconds - so 7000 is 7 seconds )
	* `[ic_do_slider use_nav_arrows=true]` - Turn on previous and next navigation arrows
	* `[ic_do_slider use_button_img=true]` - Turn on slide indicator buttons
	* `[ic_do_slider use_styles=true]` - Turn on default styles ( in case you don't add your own )
	
== Installation ==

You can install from within WordPress using the _Plugin/Add New_ feature, or if you wish to manually install:

1. Download the plugin.
1. Upload the entire `ivycat-ajax-slider` directory to your plugins folder.
1. Activate the plugin in your WordPress plugin page.
1. Start creating and embedding image sliders.

== Usage ==

= Overview =

*Here's the gist:* you create a slider group; add slides and assign them to the group and give them an order; then use simple shortcodes to embed a slider in a page or post.

= Create a Slider Group =

To create a slider group, go to Sliders/Slider Groups and add a new group category.

For example, if you wanted to have a specific slider that is shown an _About Us_ page, you might create a group called _About Us_ so you can easily identify it in the future.

= Add Slides =

Adding slides is as easy as:

1. Go to Sliders / New Slider - this will allow you to create a single slide that will appear in a slider category.
1. Give your slide a title.
1. If you wish to show text (or render HTML) in the body of the slider, you can put your text or markup in the main body of the slide post.
1. Give this slide an order in its group.  If you want it to show first in your _About Us_ slider, set _Order_ to 1 under _Slider Data_.
1. Assign the slide to a group.  If you haven't already created a group, click on the _Add New Category_ link in the Slider Groups box and add one.
1. Upload your slider image using the WordPress Media Uploader, if it hasn't already been uploaded.
1. Set the Featured Image for each slide to display your desired slider image.

**Note:** You can embed an image in step 3 above, but this isn't the intended way to add your slider images.  The better way is to set the featured image for each slide.

= Embed a slider in a page or post using shortcodes: =

`[ic_do_slider]` - Adds a slider, but only if there is a slider post tagged with a group 'home-slider'. Defaults to three slides of medium size. (as defined by Media Settings in WordPress Dashboard)

`[ic_do_slider group='my-created-group']` - Adds slides from a custom group.  Defaults to 3 slides of medium size 

`[ic_do_slider quantity='5']` - changes default quantity to 5.  (group is required if you don't have a home-slider group created)

`[ic_do_slider thumb_size='size']` - Where size is thumbnail, medium, large, full or a string in the format w,h (width,height) - see get_the_post_thumbnail() in the WP Codex for more information.

**Note:** You can combine options in your shortcode like so:

`[ic_do_slider quantity='3' thumb_size='thumbnail' group='about-us']` - Adds slides from a custom group.  Defaults to 3 slides of _medium_ size.

= Embed a slider directly in your theme template =

You can drop the following WordPress function in your template, replacing the `[shortcode]` part with your, custom shortcode.

`<?php echo do_shortcode("[shortcode]"); ?>`

In the About Us example above, you'd use:

`<?php echo do_shortcode("[ic_do_slider quantity='3' thumb_size='thumbnail' group='about-us']"); ?>`

== Screenshots ==

1. The slides are a custom post type.
2. Adding new slides is easy.
3. Add a group, or category, for your sliders.
4. Embed a slide in a post or page using a shortcode.

== Frequently Asked Questions ==

= What is the point of this plugin? =  

It allows you to easily create nice, JavaScript image sliders with text that can be styled to suit any project.

Some other slider plugins had more features than we needed, or didn't operate as we expected. Often, they load all the images at once, hiding the others and using JavaScript to switch the visible image.

This plugin is different; it loads a single image/post and once the page is loaded, makes an AJAX request to retrieve the rest of them.  It receives them in JSON format and the individual elements are switched rather than any hiding/showing going on. 

= The slider looks plain in my page - can I pick from different presentations or looks? =

Currently this plugin is more for developers or designers.  It doesn't take much to use, but it does require you to do your own styling.

Eventually we'll incorporate templating (so you can alter the markup without fear of future versions overwriting them). 

= What if I don't know CSS? =

Talk to your favorite WordPress designer or developer, or we can certainly work with you. Future versions of this plugin might support automatic features and other tweaks.

== Changelog ==

= 1.1.2 =
* Fixed html validation issue with multiple class declarations and added alt attribute to image.

= 1.04 =
* Fixed errors in pulling from groups.
* Changed slider order so lower numbers load first.

= 1.03 =
* Fixed error if group set to home-slider.

= 1.02 =
* Fix IE bug causing slider not to transition.

= 1.01 =
* Basic housekeeping, updated readme.txt, plugin header.

== Upgrade Notice ==

= 1.04 =
Important bug fixes - please udate.

= 1.03 =
Bug fixes

= 1.02 =
Important fix for Internet Explorer

= 1.01 =
No critical updates, just housekeeping.

== Road Map ==

1. Suggest a feature...