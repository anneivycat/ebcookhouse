<?php

/**
 *  Plugin Name: IvyCat AJAX Image Slider
 *  Plugin URI: http://wordpress.org/extend/plugins/ivycat-ajax-slider/
 *  Description: Add an image slider to any page.  Meant for designers and developers; no native styles.
 *  Author: IvyCat Web Services
 *  Author URI: http://www.ivycat.com
 *  version: 1.1.2
 *  License: GNU General Public License v2.0
 *  License URI: http://www.gnu.org/licenses/gpl-2.0.html
 
 ------------------------------------------------------------------------
	IvyCat AJAX Slider, Copyright 2012 IvyCat, Inc. (admins@ivycat.com)
	
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */

if ( ! function_exists( 'add_action' ) )
	wp_die( 'You are trying to access this file in a manner not allowed.', 'Direct Access Forbidden', array( 'response' => '403' ) );

if ( ! defined( 'ICSLIDE_DIR' ) )
	define( 'ICSLIDE_DIR', plugin_dir_path( __FILE__ ) );
	
if ( ! defined( 'ICSLIDE_URL' ) )
	define( 'ICSLIDE_URL', plugin_dir_url( __FILE__ ) );

class IvyCatSlider {
    
    public function __construct() {
        add_action( 'init', array( $this, 'post_type_init' ) );
    }
    
    public function post_type_init( ) {
        $labels = array(
            'name'              		=> __( 'Slides', 'post format general name', 'ivycat-ajax-slider' ),
            'singular_name' 		=> __( 'Slide', 'post format singular name', 'ivycat-ajax-slider' ),
            'add_new'         			=> __( 'New Slide', 'ivycat-ajax-slider' ),
            'add_new_item' 			=> __( 'Add New Slide', 'ivycat-ajax-slider' ),
            'edit_item'        			=> __( 'Edit Slide', 'ivycat-ajax-slider' ),
            'new_item'        		=> __( 'Add New Slide', 'ivycat-ajax-slider' ),
            'view_item'       			=> __( 'View Slide', 'ivycat-ajax-slider' ),
            'search_items'   		=> __( 'Search Slides', 'ivycat-ajax-slider' ),
			'not_found'          		=> __( 'No slides found.', 'ivycat-ajax-slider' ),
            'not_found_in_trash' 	=> __( 'No slides found in Trash.', 'ivycat-ajax-slider' ),
            'all_items'          		=> __( 'All Slides', 'ivycat-ajax-slider' ),
            'menu_name'          	=> __( 'Slides', 'ivycat-ajax-slider' )
        );
        
        $args = apply_filters( 'ic_register_slider_args', array(
            'label'                      	   => __( 'Slides' ),
            'labels'                    	   => $labels,
            'public'                    	   => true,
            'publicly_queryable' 	   => true,
            'show_ui'                 	   => true, 
            'query_var' 				   => true,
            'register_meta_box_cb' => array( $this, 'metabox_init' ),
            'rewrite' 					   => true,
            'capability_type' 		   => 'post',
            'hierarchical' 			   => false,
            'menu_position' 		   => 4,
            'supports' 					   => array( 'title', 'editor', 'thumbnail' )
        ) );

        register_post_type( 'sliders', $args );
        
		
		$tax_labels = array(
            'name'                       				=> _x( 'Slide Groups', 'taxonomy general name', 'ivycat-ajax-slider' ),
            'singular_name'              			=> _x( 'Slide Group', 'taxonomy singular name', 'ivycat-ajax-slider' ),
            'search_items'               			=> __( 'Search Slide Groups', 'ivycat-ajax-slider' ),
            'popular_items'             				=> __( 'Popular Slide Groups', 'ivycat-ajax-slider' ),
            'all_items'                  				=> __( 'All Slide Groups', 'ivycat-ajax-slider' ),
            'parent_item'                				=> __( 'Parent Slide Groups', 'ivycat-ajax-slider' ),
            'parent_item_colon'          			=> __( 'Parent Slide Group:', 'ivycat-ajax-slider' ),
            'edit_item'                  				=> __( 'Edit Slide Group', 'ivycat-ajax-slider' ),
            'view_item'                  				=> __( 'View Slide Group', 'ivycat-ajax-slider' ),
            'update_item'                			=> __( 'Update Slide Group', 'ivycat-ajax-slider' ),
            'add_new_item'               			=> __( 'Add New Slide Group', 'ivycat-ajax-slider' ),
            'new_item_name'                      	=> __( 'New Slide Group Name', 'ivycat-ajax-slider' ),
            'separate_items_with_commas'	=> __( 'Separate slide groups with commas', 'ivycat-ajax-slider' ),
            'add_or_remove_items'        		=> __( 'Add or remove slide groups', 'ivycat-ajax-slider' ),
            'choose_from_most_used'      		=> __( 'Choose from most used slide groups', 'ivycat-ajax-slider' )
        );
		
        $tax = apply_filters( 'ic_register_slider_groups_args', array(
			'hierarchical'    => true,
            'labels'             => $tax_labels,
            'rewrite'           => true
		) );
		
        register_taxonomy( 'slider-group', 'sliders', $tax );
		
        add_action( 'save_post' , array( &$this, 'save_slider_metadata' ), 10, 2 );
        add_shortcode( 'ic_do_slider', array( &$this, 'do_slider' ) );
        add_action( 'wp_ajax_get-slides',  array( &$this, 'get_slider_images' ) );
        add_action( 'wp_ajax_nopriv_get-slides',  array( &$this, 'get_slider_images' ) );
    }
    
    public function metabox_init( ) {
        add_meta_box(
            'Sliderinfo-meta',
            'Slider Data',
            array( &$this, 'slider_metabox' ),
            'sliders', 'side', 'high'
        );
    }
    
    public function slider_metabox( $post ) {
        $slider_order = get_post_meta( $post->ID, 'ivycat_slider_order', true ); ?>
		<ul>
			<li>
				<label>Order: </label>
				<input type="text" name="slider_order" value="<?php echo ( $slider_order ) ? $slider_order : '0'; ?>" />
			</li>
		</ul><?php
		do_action( 'ic_slider_order_metabox', $post );
		wp_nonce_field( 'save-slider-order_' . $post->ID, 'ivycat_slider_order_nonce' );
    }
    
    public function save_slider_metadata( $post_id, $post ) {
        if ( defined( 'DOING_AJAX') )
			return;
		 if ( ! isset( $_POST['ivycat_slider_order_nonce'] ) || ! wp_verify_nonce( $_POST['ivycat_slider_order_nonce'], 'save-slider-order_' . $post_id ) )
            return;
		do_action( 'ic_slider_save_metadata', $post_id, $post );
        update_post_meta( $post_id, 'ivycat_slider_order', absint( $_POST['slider_order'] ) );
    }
    
    public function do_slider( $atts ) {
		extract( shortcode_atts( array(
			'quantity'           => 3,
			'group'              => '0',
			'thumb_size'         => 'full',
			'fadeIn'             => 500,
			'fadeOut'            => 400,
			'speed'              => 8000,
			'use_styles'         => false,
			'use_nav_arrows'     => false,
			'use_button_img'     => false
		), $atts ) );
		
		if( $use_styles == true )
			wp_enqueue_style( 'ics-ajax-styles', ICSLIDE_URL . 'assets/ivycat_slider_styles.css' );
			
		wp_enqueue_script( 'ics-ajax-scripts', ICSLIDE_URL . 'assets/ivycat_slider_scripts.js', array( 'jquery' ) );
        wp_localize_script( 'ics-ajax-scripts', 'ICSaconn', apply_filters( 'ic_ICSaconn_variables', array(
                'ajaxurl'     			=> admin_url( 'admin-ajax.php' ),
                'themeurl'  			=> get_bloginfo( 'stylesheet_directory' ).'/',
                'pluginurl'  			=> ICSLIDE_URL,
				'slide_quantity' 		=> $quantity,
				'slide_group' 			=> $group,
				'slide_thumb_size' 	    => $thumb_size,
				'fadeIn' 				=> (int) $fadeIn,
				'fadeOut' 				=> ( int) $fadeOut,
				'slide_speed' 		    => ( int) $speed
            ) )
        );
        $slides = self::get_slides( $quantity, $group, $thumb_size ); 
        $slider_content = '<div id="ivycat-slider">'; 
		$slider_content .= self::prev_arrow( $quantity, $use_nav_arrows );
		$slider_content .= self::button_imgs( $quantity, $use_button_img ); 
        $slider_content .= '<div class="screen">'; 
        if ( strlen( $slides[0]['slide_image'] ) > 1 ) : 
			$slider_content .= '<img class="img-main" src="' . $slides[0]['slide_image'] . '"';
            $slider_content .= ' height="' . $slides[0]['slide_image_height'] . '"'; 
            $slider_content .= ' width="' . $slides[0]['slide_image_width']. '" alt="' . $slides[0]['slide_title'] . '"/>';
        endif; 
        if ( ! isset( $atts['hide_title'] ) ) : 
			$slider_content .= '<h3>' . $slides[0]['slide_title'] . '</h3>';
		endif; 
        $slider_content .= '<div class="slider-content">'. $slides[0]['slide_content'] . '</div>';
		$slider_content .= self::next_arrow( $quantity, $use_nav_arrows ); 
        $slider_content .= '</div>
            </div>';
        return $slider_content;
    }
    
    public function get_slider_images( ) {
        $quantity = $_POST['quantity'];
        $group = $_POST['group'];
        $thumb_size = $_POST['thumb_size'];
        $slides = self::get_slides( $quantity, $group, $thumb_size );
        echo json_encode( $slides );
        wp_die( false );
    }
    
    public function get_slides( $quantity , $group, $thumnail_size ) {
        $args = array(
            'post_type'         => 'sliders',
            'orderby'            => 'meta_value_num',
            'meta_key'         => 'ivycat_slider_order',
            'order'                => 'ASC',
            'posts_per_page' => $quantity
        ) ;
		
        if ( '0' != $group ):
            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'slider-group',
                    'field'          => ( is_numeric( $group ) ) ? 'id' : 'slug',
                    'terms'        => $group
                )
			);
        endif;
        $slides = get_posts( $args );
        $slider_data = array( );
        foreach ( $slides as $row ){
            $img = wp_get_attachment_image_src( get_post_thumbnail_id( $row->ID ), $thumnail_size );
            $slider_data[] = array(
                'slide_id'                 => $row->ID,
                'slide_title'              => $row->post_title,
                'slide_content'         => $row->post_content,
                'slide_image'           => ($img) ? $img[0] : '',
                'slide_image_width'  =>  ($img) ? $img[1]: '',
                'slide_image_height' =>  ($img) ? $img[2]: '',
                'slide_link'                => get_post_meta( $row->ID, 'ivycat_slider_link', true )
            );
        }
        return apply_filters( 'ic_slider_data', $slider_data );
    }
	
	protected function prev_arrow( $quantity, $show_nav ){
		if ( ! $show_nav )
			return '';
		if( $quantity > 1 ):
			$img_url = apply_filters( 'ic_slider_prev_button', ICSLIDE_URL . '/assets/btn-slider-lft.png' ); 
			return '<a class="prev" href="#">
                <img class="button btn-left" src="' . $img_url . '" alt="Previous Slider"/>
            </a>';
		endif; 
	}
	
	protected function next_arrow( $quantity, $show_nav ){
		if ( ! $show_nav )
			return '';
		if( $quantity > 1 ): 
			$img_url = apply_filters( 'ic_slider_next_button', ICSLIDE_URL . '/assets/btn-slider-rt.png' );
			return '<a class="next" href="#">
                <img class="button btn-right" src="'. $img_url .'" alt="Next Slider"/>
            </a>';
		endif; 
	}
	
	protected function button_imgs( $quantity, $show_btn ){
		if ( ! $show_btn )
			return ''; 
        $btn_img = '<div class="button page-btn">';
		for( $i = 1; $i < $quantity; $i++ ):
			$inactive_url = ICSLIDE_URL . '/assets/sldrdot-inactive.png';
			$active_url = ICSLIDE_URL . '/assets/sldrdot-active.png'; 
			$image = ( $i == 1 ) ? $active_url : $inactive_url;
			$btn_img .= '<a class="dot-slide" href="#<?php echo $i-1; ?>">
				<img id="sldrdot-' . $i . '" src="' . $image . '" alt="Switch Current Slider"/>
			</a>';
		endfor; 
        $btn_img .= '</div>';
        return $btn_img;
	}
    
}

/**
* Instantiate the Plugin - called using the plugins_loaded action hook.
*/
function init_ic_ivycat_slider( ) {
	new IvyCatSlider( );
}

add_action( 'plugins_loaded', 'init_ic_ivycat_slider' );