jQuery( 'document' ).ready( function( $ ){
    (function() {
		ics_play = false;
		slides = false;
        $.ivycat_ajax_do = function( ajaxData, callback ){
            return $.post( ICSaconn.ajaxurl, ajaxData, callback );       
        }
       
        if( $( '#ivycat-slider' ).length ){
            var slide_start = 1;
             $.ivycat_ajax_do(
				{ 'action' : 'get-slides', 'group' : ICSaconn.slide_group, 'quantity' : ICSaconn.slide_quantity, 'thumb_size' : ICSaconn.slide_thumb_size },
				function( resp ){
					slides = $.parseJSON( resp );
					current = 0;
					next = current+1;
					total = slide_length();
					rotateSwitch(  );
				}
			);
			
			$( '.btn-left' ).click( function(){
				if( current == 0 ){
					current = total - 1;
					next = current+1;
				}else{
					next = current;
					current = current-1;
				}
				increment_slider( current );
				clearInterval(ics_play);
				rotateSwitch();
				change_slider_button( next );
				return false;
			} );
			
			$( '.btn-right' ).click( function(){
				advance_slideshow();
				clearInterval(ics_play);
				rotateSwitch();
				return false;
			} );
			
			$( '.dot-slide' ).click( function(){
				var slide = $( this ).attr( 'href' ).replace( '#', '' );
				var new_url = $().attr( 'src' );
				current = parseInt( slide );
				next = parseInt( slide ) + 1;
				increment_slider( current );
				change_slider_button( next );
				clearInterval(ics_play);
				rotateSwitch();
				return false;
			});
            
			function set_container_height( height ){
				$( '#ivycat-slider .screen' ).css( {
					'height' : height
				});
				$( '#ivycat-slider' ).css( {
					'height' : height
				});
			}
			
			function advance_slideshow(){
				if( total < 2 ) return;
				
				if( total == next ){
					current = 0;
					next = 1;
				}else{
					current += 1;
					next = current+1;
				}
				increment_slider( current );
				change_slider_button( next );
			}
			
			function change_slider_button( next ){
				var url = ICSaconn.pluginurl + 'assets/sldrdot-inactive.png'
				$( '.page-btn a img' ).attr( 'src', url );
				$( '.page-btn #sldrdot-' + next ).attr( 'src', url.replace( 'inactive', 'active' ) );
			}
			
			function increment_slider( current ){
				set_container_height( slides[current].slide_image_height );
				$( '#ivycat-slider .screen' ).css( { 'height' : slides[current].slide_image_height, 'width' : slides[current].slide_image_width } );
				$( '#ivycat-slider .screen' ).customFadeOut( ICSaconn.fadeOut, function(){
					if( slides[current].slide_image.length > 1 ){
						if( $( '#ivycat-slider .screen img.img-main' ).length < 1 ){
							$( '#ivycat-slider .screen').prepend( '<img class="main" src=""/>' );
						}
						$( '#ivycat-slider .screen img.img-main' ).attr( 'src', slides[current].slide_image  );
						$( '#ivycat-slider .screen img.img-main' ).attr( 'width', slides[current].slide_image_width  );
						$( '#ivycat-slider .screen img.img-main' ).attr( 'height', slides[current].slide_image_height  );
					}else{
						$( '#ivycat-slider .screen img.img-main' ).remove();
					}
                    $( '#ivycat-slider h3' ).html( slides[current].slide_title  );
                    $( '#ivycat-slider div.slider-content' ).html( slides[current].slide_content  );
                    //$( '#ivycat-slider a' ).attr( 'href', slides[current].slide_link  );
                    
				});
				$( '#ivycat-slider .screen' ).customFadeIn( ICSaconn.fadeIn, function(){
					 
				});
			}
			
            rotateSwitch = function( ){
                ics_play = setInterval(function( ){ //Set timer - this will repeat itself every 8 seconds
                    if( total > 0 ) advance_slideshow();
                }, ICSaconn.slide_speed ); //Timer speed in milliseconds (8 seconds)
            };
            
        }
        
        function slide_length(){
                var count = 0;
                $.each( slides, function(){ count +=1; });
                return count;
            };
        
        //alert($.browser.version);
        $.fn.customFadeIn = function(speed, callback) {
            $(this).fadeIn(speed, function() {
                if(jQuery.browser.msie)
                    $(this).get(0).style.removeAttribute('filter');
                if(callback != undefined)
                    callback();
            });
        };
        $.fn.customFadeOut = function(speed, callback) {
            $(this).fadeOut(speed, function() {
                if(jQuery.browser.msie)
                    $(this).get(0).style.removeAttribute('filter');
                if(callback != undefined)
                    callback();
            });
        };
    })()
});