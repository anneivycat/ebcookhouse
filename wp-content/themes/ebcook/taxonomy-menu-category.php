<?php get_header();
	global $wp_query;
	$category = get_term_by( 'slug', $wp_query->query_vars['menu-category'], 'menu-category' );
	
	$children = ( is_object( $category ) && count( $category ) > 0 )
		? get_terms(  'menu-category' , array( 'parent' => $category->term_id ) )
		: false;
	if( $children ){
		$new_children = array();
		$ordered_children = array();
		foreach( $children as $child ){
			$order = ( $order_data = get_taxonomy_term_type( 'menu-category', $child ) ) ? $order_data : '0';
			$ordered_children[$order][] = $child;
		}
		ksort( $ordered_children );
		//fprint_r( $ordered_children );
		foreach( $ordered_children as $ochild ){
			foreach( $ochild as $oc ) {
				$new_children[] = $oc;
			}
		}
		$children = $new_children;
	}
?>
  
    <div id="content" class="<?php echo CONTAINER_CLASSES; ?>">
   
      <div id="main" class="<?php echo FULLWIDTH_CLASSES; ?>" role="main">
          <header class="well">
               <?php
               echo apply_filters( 'taxonomy-images-queried-term-image', '' ); ?>
               <h1><?php echo apply_filters( 'the_title', sanitize_text_field( $category->name ), $category->term_id ); ?></h1>
               <div class="menu-category-description">
                    <?php echo apply_filters( 'the_content' , $category->description ); ?>
               </div>
          </header>
        <?php if (!have_posts()) { ?>
          <div class="alert alert-block fade in">
			<a class="close" data-dismiss="alert">&times;</a>
			<p><?php _e('Sorry, no results were found.', 'roots'); ?></p>
          </div>
          <?php get_search_form(); ?>
        <?php } ?>
        <div class="menu-cat clearfix"><?php
		if ( ! $children ):
			global $query_string;
			query_posts( $query_string . '&posts_per_page=-1' ); ?>
              <ul class="menu-items"><?php
			$content = array();
			while ( have_posts() ) : the_post();
				$order = ( $do = get_post_meta( $post->ID, 'menu_item_display_order', true ) ) ? $do : 0 ;
				ob_start(); ?>
					<li>
						  <h3><?php the_title(); ?></h3>
						<?php the_content(); ?>
						<p class="price"><?php $price = get_post_meta( $post->ID, 'menu_item_price', true ); 
						if( is_numeric( $price ) || is_float( $price ) )
							echo  '$ '. money_format( '%.2n', $price ); ?></p>
					</li><?php
				$content[ $order ][] = ob_get_contents();
				ob_end_clean();
			endwhile; ?>
			  <?php
			ksort( $content );
			foreach( $content as $output_arr ){
				foreach( $output_arr as $output ){
					echo $output;
				}
			} ?>
			</ul><?php
		else:
			foreach( $children as $child ){
				$post_args = array(
					'post_type' => 'menu-item',
					'post_status' => 'publish',
					'posts_per_page' => -1,
                         'orderby' => 'title',
                         'order' => 'ASC',
					'tax_query' => array(
						array(
							'taxonomy' => 'menu-category',
							'field' => 'id',
							'terms' => $child->term_id
						)
					)
				);
				$menu_items = new WP_Query( $post_args );
				if( $menu_items->have_posts() ): ?>
                    <section class="sub-cat <?php echo apply_filters( 'the_title', $child->name ); ?>">
					<h2><?php echo apply_filters( 'the_title', $child->name ); ?></h2>
					<div class="desc"><?php echo apply_filters( 'the_content', $child->description ); ?></div>
                         <ul class="menu-items"><?php
					while( $menu_items->have_posts() ): $menu_items->the_post(); ?>
                              <li>
                                   <h3><?php the_title(); ?></h3>
                                   <?php the_content(); ?>
                                   <p class="price"><?php $price = get_post_meta( $post->ID, 'menu_item_price', true ); 
                                 if( is_numeric( $price ) || is_float( $price ) )
                                     echo  '$ '. money_format( '%.2n', $price ); ?></p>
                              </li>
					<?php 
					endwhile; ?>
                         </ul>
						 <?php
							if ( has_term( 'wine', 'menu-category') ) {    
								echo '<p class="note">*available by the bottle only</p>';
							}
							
							?>
						 <?php
							if ( has_term( 'salads', 'menu-category') ) {    
								echo '<p class="note">*Consuming raw or undercooked meats, poultry, seafood, shellfish, or eggs may increase your risk of food borne illness, especially if you have a medical condition.</p>';
							}
							
							?>
                    </section>
				<?php endif;
			}
		endif; ?>
		<?php
							if ( has_term(  array( 'burger-bar', 'fire-grilled' ), 'menu-category') ) {    
								echo '<p class="note">*Consuming raw or undercooked meats, poultry, seafood, shellfish, or eggs may increase your risk of food borne illness, especially if you have a medical condition.</p>';
							}		
							
							?>
		<a class="button alignright" href="<?php echo home_url(); ?>/menus/">Back to Menus &rarr;</a>
        </div>
        
      
      </div><!-- /#main -->
   
    
    </div><!-- /#content -->
 
<?php get_footer(); ?>
