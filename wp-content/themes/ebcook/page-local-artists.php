<?php get_header();
global $wp_query;
global $mappress;
?>
  
    <div id="content" class="<?php echo CONTAINER_CLASSES; ?>">
   
      <div id="main" class="<?php echo MAIN_CLASSES; ?> " role="main">
        <h1>Local Artists</h1>
        <?php
			while( have_posts() ): the_post();
				the_content();
			endwhile;
		?>
        <?php
        $args = array(
            "post_type" => "partners",
            "tax_query" => array(
                array(
                    "taxonomy" => "partner-relationship",
                    "field" => "slug",
                    "terms" => "artist"
                )
            )
        );
        $artists = new WP_Query( $args );
        if( $vendors->posts ): ?>    
            <section>
                <?php foreach( $artists->posts as $artist ):
                    $artist_data = get_post_meta( $vendor->ID, 'ebc_partner_info', true );
                ?>
                    <article class="vendor">
                        <header>
                            <a href="<?php echo home_url( '/partners/' . $artist->post_name ); ?>">
                                <?php echo get_the_post_thumbnail( $artist->ID ); ?>
                            </a>
                        </header>
                        <div>
                            <h2><a href="<?php echo home_url( '/partners/' . $artist->post_name ); ?>"><?php echo $artist->post_title; ?></a></h2>
                            <?php echo apply_filters( 'the_content', $artist->post_content ); ?>
                        </div>
                    </article>
                <?php
                if( is_object( $mappress ) ){
                    $title = "<h3>".$artist->post_title."</h3>";
                    $title .= '<ul>
                        <li>'.$artist_data['address'].'</li>
                    </ul>';
                    $mp_address[] = new Mappress_Poi( array(
                            "point" => array( "lat"=>$artist_data['lattitude'], "lng" => $artist_data['longitude'] ),
                            "title" => $title
                    ));
                }
                endforeach; ?>
            </section>
        <?php endif; ?>
      </div><!-- /#main -->
   
      <aside id="sidebar" class="<?php echo SIDEBAR_CLASSES; ?>" role="complementary">
      
        <?php
            if( is_object( $mappress ) && $artists->posts ):
                $mymap = new Mappress_Map( array( "width" => 300, "Title"=>"Search Results" ) );
                $mymap->pois =$mp_address;
                echo $mymap->display();
            endif;
        ?>
        
      </aside><!-- /#sidebar -->
    
    </div><!-- /#content -->
 
<?php get_footer(); ?>
