<?php get_header();
global $wp_query;
global $mappress;
?>
  
    <div id="content" class="<?php echo CONTAINER_CLASSES; ?>">
   
      <div id="main" class="<?php echo MAIN_CLASSES; ?> menu-main" role="main">
        <h1>Local Vendors</h1>
        <?php
			while( have_posts() ): the_post();
				the_content();
			endwhile;
		?>
        <?php
        $args = array(
            "post_type" => "partners",
            "tax_query" => array(
                array(
                    "taxonomy" => "partner-relationship",
                    "field" => "slug",
                    "terms" => "vendor"
                )
            )
        );
        $vendors = new WP_Query( $args );
        if( $vendors->posts ): ?>    
            <section>
                <?php foreach( $vendors->posts as $vendor ):
                    $vendor_data = get_post_meta( $vendor->ID, 'ebc_partner_info', true );
                ?>
                    <article class="vendor">
                        <header>
                            <a href="<?php echo home_url( '/partners/' . $vendor->post_name ); ?>">
                                <?php echo get_the_post_thumbnail( $vendor->ID ); ?>
                            </a>
                        </header>
                        <div>
                            <h2><a href="<?php echo home_url( '/partners/' . $vendor->post_name ); ?>"><?php echo $vendor->post_title; ?></a></h2>
                            <?php echo apply_filters( 'the_content', $vendor->post_content ); ?>
                        </div>
                    </article>
                <?php
                if( is_object( $mappress ) ){
                    $title = "<h3>".$vendor->post_title."</h3>";
                    $title .= '<ul>
                        <li>'.$vendor_data['address'].'</li>';
                    if( strlen( $vendor_data['address2'] ) ) $title .= '<li>'.$vendor_data['address2'].'</li>';
                    $title .= '<li>'.$vendor_data['city'].', '. $vendor_data['state'] .' ' . $vendor_data['zipcode'] .'</li>';
                    $title .= '<li><a href="https://maps.google.com/?daddr='.$vendor_data['address'].'+'.$vendor_data['city'].'+'.$vendor_data['state'].'+'.$vendor_data['zipcode'].' " target="_blank">
                    get directions</a></li>';
                    $title .= '</ul>';
                    $mp_address[] = new Mappress_Poi( array(
                            "point" => array( "lat"=>$vendor_data['lattitude'], "lng" => $vendor_data['longitude'] ),
                            "title" => $title
                    ));
                }
                endforeach; ?>
            </section>
        <?php endif; ?>
      </div><!-- /#main -->
   
      <aside id="sidebar" class="<?php echo SIDEBAR_CLASSES; ?>" role="complementary">
      
        <?php
            if( is_object( $mappress ) ):
                $mymap = new Mappress_Map( array( "width" => 300, "Title"=>"Search Results" ) );
                $mymap->pois =$mp_address;
                echo $mymap->display();
            endif;
        ?>
        
      </aside><!-- /#sidebar -->
    
    </div><!-- /#content -->
 
<?php get_footer(); ?>
