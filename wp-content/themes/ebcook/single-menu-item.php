<?php get_header(); ?>
  
    <div id="content" class="<?php echo CONTAINER_CLASSES; ?>">
    
      <div id="main" class="menu-main <?php echo MAIN_CLASSES; ?>" role="main">
        
        <?php /* Start loop */ ?>
        <?php while (have_posts()) : the_post(); ?>
          
            <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
            
              <header>
              
                <?php
                    if( $img = get_the_post_thumbnail( $post->ID, 'thumbnail' ) ){
                        echo $img;
                    }else{
                        echo wp_get_attachment_image( 117 ); 
                    }
                ?>  
              </header>
              
                <section>
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                    <?php the_content(); ?>
                    <p class="price">
                    <?php $price = get_post_meta( $post->ID, 'menu_item_price', true ); 
                    if( is_numeric( $price ) || is_float( $price ) )
                        echo  '$ '. money_format( '%.2n', $price ); ?>
                    </p>
                </section>
             
              <footer>
                
              </footer>
              
            </article>
        </div><!-- /#main -->
    
      <aside id="sidebar" class="<?php echo SIDEBAR_CLASSES; ?>" role="complementary">
            <?php $nutrition_data_point = array( 'serving_size', 'servings', 'calories', 'unsat_fat', 'sat_fat', 'carbs', 'protein' );
                foreach( $nutrition_data_point as $row ){
                    if( $item_data = get_post_meta( $post->ID, 'menu_item_' . $row, true ) ) $nutrition_data[$row] =$item_data;
                }
            if( count( $nutrition_data ) == 7 ): ?>
            <ul>
                <li>Serving Size: <?php echo $nutrition_data['serving_size']; ?></li>
                <li>Servings: <?php echo $nutrition_data['servings']; ?></li>
                <li>Calories: (kcal) <?php echo $nutrition_data['calories']; ?></li>
                <li>Unsaturated Fat: <?php echo $nutrition_data['unsat_fat']; ?></li>
                <li>Saturated Fat: <?php echo $nutrition_data['sat_fat']; ?></li>
                <li>Carbohydrates: <?php echo $nutrition_data['carbs']; ?></li>
                <li>Protein: <?php echo $nutrition_data['protein']; ?></li>
            </ul>
            <?php
            endif; ?>
      </aside><!-- /#sidebar -->
          
        <?php endwhile; /* End loop */ ?>
    
    </div><!-- /#content -->
  
<?php get_footer(); ?>
