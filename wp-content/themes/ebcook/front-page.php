<?php get_header(); ?>
  
    <div id="content" class="<?php echo CONTAINER_CLASSES; ?>">
    
      <div id="main" class="<?php echo FULLWIDTH_CLASSES; ?>" role="main">
        <section class="slider">
          <?php echo do_shortcode( '[ic_do_slider group="home-page" thumb_size="full" use_nav_arrows=true quantity="5"]' ); ?>
        </section>
        
        <?php /* Start loop */ ?>
        <?php while (have_posts()) : the_post(); ?>
        <div class="page-header tagline">
          <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/img/tagline.png" alt="Everybody's Somebody at Everybody's"/>
        </div>
        <section id="home-wrap">
          <?php the_content(); ?>
        </section>
        <?php endwhile; /* End loop */ ?>

      </div><!-- /#main -->
      
    </div><!-- /#content -->
    <section class="signup green-ribbon">
        <ul>
            <li><!-- Begin MailChimp Signup Form -->
              
              
              <div id="mc_embed_signup">
                <h3 for="mce-EMAIL">SIGNUP for Updates and Specials</h3>
              <form action="http://ebcookhouse.us5.list-manage1.com/subscribe/post?u=22ad3d860c17e538e8024c229&amp;id=64ea7e6a7d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                  
                  <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="please add your email" required>
                  <input type="submit" value="" name="subscribe" id="mc-embedded-subscribe" class="button">
              </form>
              </div>
              
              <!--End mc_embed_signup--></li>
            <!--<li><h3>LOYALTY PROGRAM - <a href="<?php echo home_url(); ?>/loyalty-program">Eat and Earn at Everybody's</h3></a>
            </li>-->
        </ul>
      </section>
<?php get_footer(); ?>
