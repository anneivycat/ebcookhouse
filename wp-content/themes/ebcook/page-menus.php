<?php get_header(); ?>
  
    <div id="content" class="<?php echo CONTAINER_CLASSES; ?>">
   
      <div id="main" class="<?php echo FULLWIDTH_CLASSES; ?> menu-main" role="main">
        <h1>Our Menu</h1>
        <?php the_content ?>
        <ul class="menus">
        <?php $cats = get_terms( 'menu-category', array( 'hide_empty' => false, 'hierarchical' => false ) );
        if( $cats ):
			$menu_categories = array();
			foreach( $cats as $category ){
				$order = ( $order_data = get_taxonomy_term_type( 'menu-category', $category ) ) ? $order_data : '0';
				$menu_categories[ $order ][] = $category;
			}
			ksort( $menu_categories );
			$cats = array();
			foreach( $menu_categories as $ctgy ){
				foreach( $ctgy as $term ) {
					$cats[] = $term;
				}
			}
			$tax_img = get_option( 'taxonomy_image_plugin' ); ?>
            <?php foreach( $cats as $cat ):
			if( 0 != $cat->parent )
			continue; ?>
            <li class="menu">
                        <a class="menu-thumb" href="<?php echo home_url( '/menu-category/' ) . $cat->slug . '/'; ?>">
                            <?php $img_id = ( $tax_img[$cat->term_id] ) ? $tax_img[$cat->term_id] : 117 ;
                                  $size = 75;
                            echo wp_get_attachment_image( $img_id, array($size, $size)); ?>
                        </a>
                        <a class="menu-title" href="<?php echo home_url( '/menu-category/' ) . $cat->slug . '/'; ?>">
                            <h2><?php echo $cat->name; ?></h2>
                        </a>
                        <?php //echo $cat->description; ?>
              </li>  
            <?php endforeach; ?>
      <?php endif;
      
      //fprint_r( $tax_img );
      ?>    
        </ul>
      </div><!-- /#main -->
   
    
    </div><!-- /#content -->
 
<?php get_footer(); ?>
