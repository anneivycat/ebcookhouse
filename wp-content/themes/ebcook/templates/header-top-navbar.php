<div  id="banner" class="row-fluid" role="banner">
        <section class="hours span4">
                <h3 class="hours-link"><a class="button" href="<?php echo home_url(); ?>/contact-location/">Hours</a></h3>
            <?php echo apply_filters( 'the_content', get_option( 'eb_hours' ) ); ?>
        </section>
        <section class="logo span4">
            <a href="<?php echo home_url(); ?>">
                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/img/ebc-logo.png" alt="Everybody's American Cookhouse & Sports Theater"/>
            </a>
        </section>
        <section class="links span4">
                
            <?php $business = get_option( 'eb_business' ); ?>
            <h3><?php echo $business['phone']; ?></h3>
            <ul>
                <li>Call for Reservations</li>
                <li><a href="<?php echo home_url(); ?>/menus/">View Menu</a></li>
            </ul>
            
        </section>
        
    </div><!-- end #banner -->
<section id="nav-topbar" class="navbar green-ribbon">
    <div class="navbar-inner">
       <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <nav id="nav-main" class="nav-collapse" role="navigation">
          <?php wp_nav_menu(array('theme_location' => 'primary_navigation', 'walker' => new Roots_Navbar_Nav_Walker(), 'menu_class' => 'nav')); ?>
        </nav>
      </div>
</section><!-- end #nav-topbar -->
