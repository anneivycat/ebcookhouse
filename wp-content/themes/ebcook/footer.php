
  </div><!-- /#wrap -->

  
  <footer id="content-info" class="<?php echo WRAP_CLASSES; ?>" role="contentinfo">
    <div class="wrap">
      
      <section class="footer-top">
        
      <?php dynamic_sidebar('sidebar-footer-top'); ?>
      </section>
      <section class="footer-bottom">
      <?php dynamic_sidebar('sidebar-footer-bottom'); ?>
      </section>
      <p class="copy"><small>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></small></p>
    </div>
  </footer>
  <?php wp_footer(); ?>
</body>
</html>
