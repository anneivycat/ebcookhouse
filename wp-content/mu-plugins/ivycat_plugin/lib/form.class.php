<?php

require_once 'validate.class.php';

/**
*   Validation "controller" of sorts.  Almost a Factory class with some common functions.  This is the front door for any controller to
*   access a set of validation rules and methods for a particular form.
**/
class FormCreate{
    
    protected $form_data;
    protected $inputs;
    protected $post_status;
    protected $validate;
         
/**
*   Main constructor method: requires the library to be called (string)
**/   
    public function __construct(){
        $this->validate = new ValidateForm();
        $this->post_status = ( $_SERVER['REQUEST_METHOD'] == "POST" ) ? true : false;
    }
    
    public function new_field( $type, $name, $atts ){
        $field = 'add_' . $type . '_field';
        $this->inputs[] = self::$field( $name, $atts );
    }
    
    public function new_fields( array $fields ){
        foreach( $fields as $field ){
            self::new_field( $field['type'], $field['name'], $field['atts'] );
        }
        return true;
    }
    
    public function process_form( $data ){
        if( !$this->post_status ) return;
        $this->inputs = $this->validate->validate_array( $this->inputs );
        foreach( $this->inputs as $key=>$value ){
            if( isset( $value['error'] ) ){
                $this->inputs[$key]['output'] .= '<div class="error">'.$value['error'].'</div>';
            }
        }
        return $this->inputs;
    }    
/**
*   Returns the scrubbed data back - essentially in the form of the POST data. 
*   (note: form elements not in the validation array will not be passed along this is an exclusive method )
**/   
    public function get_data(){
        $data = array();
        foreach($this->inputs as $k=>$v){
            if( is_array( $v ) ) $data[$k] = $v['value'];
        }
        //fprint_r( $this->inputs );
        return $data;
    }
         
/**
*   Returns the formdata (this has labels, rules and data)
**/   
    public function all_formdata(){
        return $this->inputs;
    }
         
/**
*   returns one record 
**/   
    public function get_field($key){
        return $this->data[$key];
    }
          
/**
*   A test to see if there are errors.  TRUE means there is an error!
**/  
    public function has_errors(){
        return $this->validate->has_errors();
    }
           
        
/**
*   Shows the errors for a given field
**/    
    public function show_errors( $field, $label = false ){
        if($error = $this->form_data[$field]['error']){
            return '<p class="error">'.$error.'</p>';  
        }
        return ( $label ) ? $label : ""; 
    }
         
/**
*   set the value of an element in form_data
**/   
    public function set_value_post($field){
        return $this->form_data[$field]['value'];
    }

/**
 *  Adds a validation rule only.
 */
    public function add_validation_rule( $name, $label, $rules='', $scrub='', $value='' ){
        $this->inputs[$name] = array(
                'label' => $label,
                'rules' => $rules,
                'scrub' => $scrub,
                'value' => ( isset( $_POST[$name] ) ) ? $_POST[$name]  : $value                             
        );
    }
    
    public function add_validation_rules( array $rules ){
        foreach( $rules as $rule ){
            self::add_validation_rule( $rule['name'], $rule['label'], $rule['rules'], $rule['scrub'], $rule['value'] );
        }
    }
    
    public function output_field( $name ){
        return $this->inputs[$name]['output'];
    }
    
    protected function add_text_field( $name, $atts ){
        $value = ( $atts['default'] ) ? $atts['default'] : ' ';
        $value = ( isset( $_POST[$name] ) ) ? $_POST[$name] : $value;
        $label = isset( $atts['label'] ) ? trim($atts['label']) : trim( ucwords( str_replace( '-', ' ', $name ) ) );
        $labelclass = ( isset( $atts['rules'] ) && preg_match( '~required~', $atts['rules'] ) ) ? ' class="required"' : '';
        $id = ( $atts['id'] ) ? trim($atts['id']) : 'input-'.trim($name);
        $class = ( $atts['class'] ) ? ' class="'.$atts['class'].'"' :  '';
        $output = '<label for="'.$id.'"'.$labelclass.'>'.$label.'</label>
            <input id="'.$id.'"'.$class.' type="text" name="'.trim($name).'" value="'.trim($value).'"/>';
        self::set_input( $name, $label, $value, $atts, $output );
    }
    
    protected function add_textarea_field( $name, $atts ){
        $value = ( isset( $_POST[$name] ) ) ? trim($_POST[$name]) : ' ';
        $label = isset( $atts['label'] ) ? trim($atts['label']) : trim( ucwords( str_replace( '-', ' ', $name ) ) );
        $labelclass = ( isset( $atts['rules'] ) && preg_match( '~required~', $atts['rules'] ) ) ? ' class="required"' : '';
        $id = ( $atts['id'] ) ? trim($atts['id']) : 'input-'.trim($name);
        $class = ( $atts['class'] ) ? ' class="'.$atts['class'].'"' :  '';
        $output = '<label for="'.$id.'"'.$labelclass.'>'.$label.'</label>
            <textarea id="'.$id.'"'.$class.' type="text" name="'.trim($name).'">'.trim($value).'</textarea>';
        self::set_input( $name, $label, $value, $atts, $output );
    }
    
    protected function add_editor_field( $name, $atts ){
        
    }
    
    protected function add_checkbox_field( $name, $atts ){
        $value = ( array ) ( $atts['default'] ) ? $atts['default'] : ' ';
        $value = ( isset( $_POST[$name] ) ) ? (array)$_POST[$name] : $value;
        $label = isset( $atts['label'] ) ? trim($atts['label']) : trim( ucwords( str_replace( '-', ' ', $name ) ) );
        $labelclass = ( isset( $atts['rules'] ) && preg_match( '~required~', $atts['rules'] ) ) ? ' class="required"' : '';
        $id = ( $atts['id'] ) ? trim($atts['id']) : 'input-'.trim($name);
        $class = ( $atts['class'] ) ? ' class="'.$atts['class'].'"' :  '';
        $itr = 1;
        $output = "<ul class='multiple-select'>";
        $output .= ( !$atts['hide-label'] ) ? $atts['label'] : '';
        foreach( $atts['options'] as $kval => $kname ):
            $val = ( is_numeric( $kval ) ) ? $kval : trim( $kval );
            if( is_array( $value ) )$checked = ( in_array( $kval, $value ) ) ? ' checked="checked"' : '';
            $output .= '<li><input id="'.$id.'-'.$itr.'"'.$class.' type="checkbox" name="'.trim($name).'[]" value="'.$val.'"'.$checked.'/>
            <label for="'.$id.'-'.$itr.'"'.$labelclass.'>'.$kname.'</label></li>';
            $itr++;
        endforeach;
        $output .= "</ul>";
        self::set_input( $name, $label, $value, $atts, $output );
    }
    
    protected function add_radio_field( $name, $atts ){
        $default = ( $atts['default'] ) ? $atts['default'] : ' ';
        $value = ( isset( $_POST[$name] ) ) ? trim( $_POST[$name] ) : $default;
        $label = isset( $atts['label'] ) ? trim($atts['label']) : trim( ucwords( str_replace( '-', ' ', $name ) ) );
        $labelclass = ( isset( $atts['rules'] ) && preg_match( '~required~', $atts['rules'] ) ) ? ' class="required"' : '';
        $id = ( $atts['id'] ) ? trim($atts['id']) : 'input-'.trim($name);
        $class = ( $atts['class'] ) ? ' class="'.$atts['class'].'"' :  '';
        $itr = 1;
        $output = "<ul>";
        $output .= ( !$atts['hide-label'] ) ? '<li>'.$atts['label'].'</li>' : '';
        foreach( $atts['options'] as $kval => $kname ):
            $val = ( is_numeric( $kval ) ) ? $kval : trim( $kval );
            $checked = ( $value == $val ) ? ' checked="checked"' : '';
            $output .= '<li><input id="'.$id.'-'.$itr.'"'.$class.' type="radio" name="'.trim($name).'" value="'.$val.'"'.$checked.'/>
            <label for="'.$id.'-'.$itr.'"'.$labelclass.'>'.$kname.'</label></li>';
            $itr++;
        endforeach;
        $output .= "</ul>";
        self::set_input( $name, $label, $value, $atts, $output );
    }
    
    protected function add_dropdown_field( $name, $atts ){
        $value = ( isset( $_POST[$name] ) ) ? trim($_POST[$name] ) : ' ';
        $label = isset( $atts['label'] ) ? trim($atts['label']) : trim( ucwords( str_replace( '-', ' ', $name ) ) );
        $labelclass = ( isset( $atts['rules'] ) && preg_match( '~required~', $atts['rules'] ) ) ? ' class="required"' : '';
        $id = ( $atts['id'] ) ? trim($atts['id']) : 'input-'.trim($name);
        $class = ( $atts['class'] ) ? ' class="'.$atts['class'].'"' :  '';
        $output = '<label for="'.$id.'"'.$labelclass.'>'.$label.'</label>';
        $output .= '<select id="'.$id.'"'.$class.' type="text" name="'.trim($name).'" value="'.trim($value).'">';
            foreach( $atts['options']  as $kval=>$kname ):
            $selected = ( $value == $kval ) ? ' selected="selected"' : '';
            $output .= '<option value="'.$abbr.'"'.$selected.'>'.$state.'</option>';
            endforeach;
        $output .= '</select>';
        self::set_input( $name, $label, $value, $atts, $output );
    }
    
    protected function add_hidden_field( $name, $atts ){
        $id = ( $atts['id'] ) ? trim($atts['id']) : 'input-'.trim($name);
        $class = ( $atts['class'] ) ? ' class="'.$atts['class'].'"' :  '';
        $output = '<input id="'.$id.'"'.$class.' type="hidden" name="'.trim($name).'" value="'.trim( $atts['value'] ).'"/>';
        self::set_input( $name, $label, $atts['value'], $atts, $output );
    }
    
    protected function add_state_field( $name, $atts ){
        $value = ( isset( $_POST[$name] ) ) ? trim($_POST[$name]) : ' ';
        $label = isset( $atts['label'] ) ? trim($atts['label']) : trim( ucwords( str_replace( '-', ' ', $name ) ) );
        $labelclass = ( isset( $atts['rules'] ) && preg_match( '~required~', $atts['rules'] ) ) ? ' class="required"' : '';
        $id = ( $atts['id'] ) ? trim($atts['id']) : 'input-'.trim($name);
        $class = ( $atts['class'] ) ? ' class="'.$atts['class'].'"' :  '';
        $output = '<label for="'.$id.'"'.$labelclass.'>'.$label.'</label>';
        $output .= '<select id="'.$id.'"'.$class.' type="text" name="'.trim($name).'" value="'.trim($value).'">';
            foreach( self::states() as $abbr=>$state ):
            $selected = ( $abbr == $value ) ? ' selected="selected"' : '';
            $output .= '<option value="'.$abbr.'"'.$selected.'>'.$state.'</option>';
            endforeach;
        $output .= '</select>';
        self::set_input( $name, $label, $value, $atts, $output );
    }
    
    protected function set_input( $name, $label, $value, $atts, $output ){
        $this->inputs[$name] = array(
            'label' => $label,
            'rules' => isset( $atts['rules'] ) ? $atts['rules'] : '',
            'scrub' => isset( $atts['scrub'] ) ? $atts['scrub'] : '',
            'value' => $value,
            'atts' => $atts,
            'output' => $output
        );
    }
    
    protected function states(){
        return array(
            "AL"=>"Alabama",
            "AK"=>"Alaska",
            "AZ"=>"Arizona",
            "AR"=>"Arkansas",
            "CA"=>"California",
            "CO"=>"Colorado",
            "CT"=>"Connecticut",
            "DE"=>"Deleware",
            "DC"=>"District of Columbia",
            "FL"=>"Florida",
            "GA"=>"Georgia",
            "HI"=>"Hawaii",
            "ID"=>"Idaho",
            "IL"=>"Illinois",
            "IN"=>"Indiana",
            "IA"=>"Iowa",
            "KS"=>"Kansas",
            "KY"=>"Kentuky",
            "LA"=>"Louisiana",
            "ME"=>"Maine",
            "MD"=>"Maryland",
            "MA"=>"Massachusetts",
            "MI"=>"Michigan",
            "MN"=>"Minnesota",
            "MS"=>"Mississippi",
            "MO"=>"Missouri",
            "MT"=>"Montana",
            "NE"=>"Nebraska",
            "NV"=>"Nevada",
            "NH"=>"New Hampshire",
            "NJ"=>"New Jersey",
            "NM"=>"New Mexico",
            "NY"=>"New York",
            "NC"=>"North Carolina",
            "ND"=>"North Dakota",
            "OH"=>"Ohio",
            "OK"=>"Oklahoma",
            "OR"=>"Oregon",
            "PA"=>"Pennsylvania",
            "RI"=>"Rhode Island",
            "SC"=>"South Carolina",
            "SD"=>"South Dakota",
            "TN"=>"Tennessee",
            "TX"=>"Texas",
            "UT"=>"Utah",
            "VT"=>"Vermont",
            "VA"=>"Virginia",
            "WA"=>"Washington",
            "WV"=>"West Virginia",
            "WI"=>"Wisconsin",
            "WY"=>"Wyoming"
        );
    }
}
