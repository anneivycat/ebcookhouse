<?php

class CustomPostType{
    
    protected $json_url;
    protected $post_types;
    
    public function __construct( ){
        $this->json_url = MU_IVYCAT_DIR . '/config/custom_post_types.json';
        self::init_cpt_data();
    }
    
    public function init(){
        self::init_cpt_data();
        foreach( $this->post_types as $type ){
            $args = (array)$type->args;
            $args['labels'] = (array)$args['labels'];
            register_post_type( $type->slug, $args );
        }
    }
    
    public function get_types(){
        return (array) $this->post_types;
    }
    
    public function get_list(){
        
    }
    
    public function new_cpt(){
        $this->form = new FormCreate();
        self::new_form_init( );
        if( $_SERVER['REQUEST_METHOD'] == 'POST' ){
            self::save( $_POST  );
        }else{
            require_once MU_IVYCAT_DIR . '/views/new_cpt_view.php';
        }
    }
    
    public function save( $cpt_data ){
        self::set_data( $cpt_data );
        $fp = fopen( $this->json_url , 'w' );
        fwrite( $fp, json_encode(  $this->post_types ) );
        fclose( $fp );
    }
    
    public function delete(){
        
    }
    
    protected function init_cpt_data(){
        $cpt = self::get_json();
        $this->post_types = ( $cpt ) ? $cpt : array();
    }
    
    protected function get_json(){
        return json_decode( file_get_contents( $this->json_url  ) );
    }
    
    protected function new_form_init( ){
        $fields = array(
            array( 'type'=>'text', 'name'=>'cpt-name', 'atts'=>array( "label" => "Name" ) ),
            array( 'type'=>'text', 'name'=>'cpt-singular-name', 'atts'=>array( "label" => "Singular Name" ) ),
            array( 'type'=>'text', 'name'=>'cpt-slug', 'atts'=>array( "label" => "Slug" ) ),
            array( 'type'=>'radio', 'name'=>'cpt-public', 'atts'=>array( "label" => "Public", "default"=> true,
                "options" => array( true => "Yes", false=>"No" ) ) ),
            array( 'type'=>'radio', 'name'=>'cpt-publicly_queryable', 'atts'=>array( "label" => "Publicly Queryable", "default"=> true,
                "options" => array( true => "Yes", false=>"No" )  ) ),
            array( 'type'=>'radio', 'name'=>'cpt-show_ui', 'atts'=>array( "label" => "Show UI", "default"=> true,
                "options" => array( true => "Yes", false=>"No"  )  ) ),
            array( 'type'=>'radio', 'name'=>'cpt-rewrite', 'atts'=>array( "label" => "Rewrite", "default"=> true,
                "options" => array( true => "Yes", false=>"No"  )  ) ),
            array( 'type'=>'radio', 'name'=>'cpt-hierarchical', 'atts'=>array( "label" => "Hierarchical", "default"=> false,
                "options" => array( true => "Yes", false=>"No"  )  ) ),
            array( 'type'=>'text', 'name'=>'cpt-menu-position', 'atts'=>array( "label" => "Menu Position", "default"=> '80' ) ),
            array( 'type'=>'checkbox', 'name'=>'cpt-supports', 'atts'=>array( "label" => "Supports", "default"=> array( 'title', 'editor', 'thumbnail' ),
                "options" => array( "title" => "Title", "editor" => "Editor", "thumbnail"=>"Thumbnail" ) ) )
        );
        $this->form->new_fields( $fields );
    }
    
    public function set_data( $arg_data ){
        $labels = array(
            'name'=>__( $arg_data['cpt-name'] ),
            'singular_name'=>__( $arg_data['cpt-singular-name'] ),
            'add_new'=>__( 'New ' . $arg_data['cpt-singular-name'] ),
            'add_new_item'=>__( 'Add New '. $arg_data['cpt-singular-name'] ),
            'edit_item'=>__( 'Edit '. $arg_data['cpt-singular-name'] ),
            'new_item'=>__( 'Add New ' . $arg_data['cpt-singular-name'] ),
            'view_item'=>__( 'View '. $arg_data['cpt-name'] ),
            'search_items'=>__( 'Search '. $arg_data['cpt-name'] )
        );
        // Change what you want it to support here.
        $supports = $arg_data['cpt-supports'];
        
        $args = array(
            'label'=> __( $arg_data['cpt-singular-name'] ),
            'labels'=>$labels,
            'public' => $arg_data['cpt-public'],
            'publicly_queryable' => $arg_data['cpt-publicly_queryable'],
            'show_ui' => true, 
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'hierarchical' => 0,
            'menu_position' => $arg_data['cpt-menu-position'],
            'supports' => $supports
        );
        $no_duplicate = true;
        foreach( $this->post_types as $key=>$value ){
            if(  $arg_data['cpt-slug'] == $value['slug']  ){
                $no_duplicate = false;
                $value['args'] = $args;
            }
        }
        if( $no_duplicate ) $this->post_types[] = array( "slug"=> $arg_data['cpt-slug'], "args" => $args );
    }
}
