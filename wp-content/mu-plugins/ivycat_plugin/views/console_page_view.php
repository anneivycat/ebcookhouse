<div class="wrap" id="page-specific-sidebar-settings">
    <div id="icon-options-general" class="icon32"></div>
    <h2>IvyCat Console</h2>
    <?php require_once MU_IVYCAT_DIR. '/views/console_menu_view.php'; ?>
    <section id="console-wrap">
        <section class="console-content" id="notes">
            <form action="" method="post">
                <h3>Add/Edit Notes for <?php bloginfo( 'name' ); ?></h3>
                <?php the_editor( get_option( 'ivycat_notes' ), 'ivycat_notes' ); ?>
                <button class="button-primary" type="submit" name="save_notes" value="Save Notes">Save Notes</button>
            </form>
        </section>
    </section>
<div>
