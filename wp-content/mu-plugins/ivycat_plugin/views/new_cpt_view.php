<div class="wrap" id="page-specific-sidebar-settings">
    <div id="icon-options-general" class="icon32"></div>
    <h2>Add New Custom Post Type</h2>
    <form method="post" action="">
        <ul>
            <?php
            foreach( $this->form->all_formdata() as $field ): ?>
                <li>
                    <?php echo $field['output'] ?>
                </li>
            <?php endforeach; ?>
            <button type="submit" name="add-new-cpt" value="save-cpt">Save</button>
        </ul>
    </form>
</div>
