<div class="wrap" id="hours-locations-settings">
    <div id="icon-options-general" class="icon32"></div>
    <h2>Hours and Locations</h2>
    <section id="console-wrap">
        <section class="console-content" id="notes">
            <form action="" method="post">
                <h3>Add/Edit Hours for <?php bloginfo( 'name' ); ?></h3>
                <?php the_editor( get_option( 'eb_hours' ), 'eb_hours' ); ?>
                <button class="button-primary" type="submit" name="save_hours" value="Save Hours">Update Hours</button>
            </form>
        </section>
    </section>
    <hr/>
    <section>
        <form action="" method="post">
            <fieldset>
                <legend><h3>Edit Location for <?php bloginfo( 'name' ); ?></h3></legend>
                <ul>
                    <li>
                        <label for="business-name">Business Name</label>
                    </li>
                    <li>
                        <input id="business-name" type="text" name="business[name]" value="<?php echo $business['name']; ?>"/>
                    </li>
                    <li>
                        <label for="address">Address</label>
                    </li>
                    <li>
                        <input id="address" type="text" name="business[address]" value="<?php echo $business['address']; ?>"/>
                    </li>
                    <li>
                        <label for="address2">Address 2</label>
                    </li>
                    <li>
                        <input id="address2" type="text" name="business[address2]" value="<?php echo $business['address2']; ?>"/>
                    </li>
                    <li>
                        <label for="city">City</label>
                    </li>
                    <li>
                        <input id="city" type="text" name="business[city]" value="<?php echo $business['city']; ?>"/>
                    </li>
                    <li>
                        <label for="state">State</label>
                    </li>
                    <li>
                        <input id="state" type="text" name="business[state]" value="<?php echo $business['state']; ?>"/>
                    </li>
                    <li>
                        <label for="zip">ZipCode</label>
                    </li>
                    <li>
                        <input id="zip" type="text" name="business[zip]" value="<?php echo $business['zip']; ?>"/>
                    </li>
                    <li>
                        <label for="phone">Phone</label>
                    </li>
                    <li>
                        <input id="phone" type="text" name="business[phone]" value="<?php echo $business['phone']; ?>"/>
                    </li>
                </ul>
            </fieldset>
            <button class="button-primary" type="submit" name="save_hours" value="Save Hours">Update Location</button>
        </form>
    </section>
<div>
