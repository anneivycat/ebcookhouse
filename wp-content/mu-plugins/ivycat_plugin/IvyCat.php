<?php

if (!function_exists ('add_action')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}


define( 'MU_IVYCAT_DIR', dirname( __FILE__ ) );
define( 'MU_IVYCAT_URL', str_replace( ABSPATH, site_url( '/' ), MU_IVYCAT_DIR) );

require_once 'lib/functions.php';
require_once 'lib/form.class.php';
require_once 'lib/CustomPostType.php';

class IvyCatClass{
    
    public function __construct(){
        add_action( 'init', array( new CustomPostType, 'init' ) );
        add_action('admin_menu', array( __CLASS__, 'options_page_init') );
        add_action( 'admin_notices', array( __CLASS__, 'ivycat_notices' ) );
        add_action( 'admin_init', array( __CLASS__, 'general_admin_assets' ) );
    }
    
    public function general_admin_assets(){
         wp_enqueue_style( 'ivycat-general-admin', MU_IVYCAT_URL . '/assets/styles/general-admin.css' );
    }
    
    public function options_page_init(){
        if( !is_ivycat() ) return;
		$hooks = array(
            add_options_page( __( 'Hours and Location' ), __( 'Hours and Location' ), 'read', 'hours-location', array( __CLASS__, 'hours_and_location' ) )
        );
        if( is_ivycat ) $hooks[] = add_options_page( __( 'IvyCat Console' ), __( 'IvyCat Console' ), 'read', 'ivycat-console', array( __CLASS__, 'console_page' ) );
        foreach( $hooks as $hook ) add_action( "admin_print_styles-{$hook}", array( __CLASS__, 'load_assets' ) );
    }
    
    public function load_assets(){
        $scripturl = '/assets/scripts/'.$_GET['page'];
        $cssurl = '/assets/styles/'.$_GET['page'];
        if( file_exists( MU_IVYCAT_DIR . $cssurl .'.css' ) ) wp_enqueue_style( $_GET['page'] . '-style', MU_IVYCAT_URL . $cssurl . '.css' );
        if( file_exists( MU_IVYCAT_DIR . $scripturl . '.js' ) ) wp_enqueue_script( $_GET['page'] . '-script', MU_IVYCAT_URL . $scripturl . '.js', array( 'jquery' ) );
    }
    
    public function hours_and_location(){
        if( $_POST['eb_hours'] ){ update_option( 'eb_hours', $_POST['eb_hours'] ); }
        if( $_POST['business'] ){ update_option( 'eb_business', $_POST['business'] ); }
        $business = get_option( 'eb_business' );
        require_once 'views/hours_location_view.php';
    }
    
    public function ivycat_notices(){
        if( !is_ivycat() ) return false;
        $notes = get_option( 'ivycat_notes' );
        if( $notes ) echo '<div id="message" class="updated ivycat-notice"><h3>IvyCat Notifications: </h3>'.stripcslashes( $notes ).'</div>';
    }
    
    public function console_page(){
        if( $_POST ) self::save_console_page();
        $url = ( $_SERVER['REQUEST_URI'] );
        if( $_GET['tab'] == 'cpt' ):
            $cpt = new CustomPostType();
            $old = $cpt->get_types();
            switch( $_GET['action'] ){
                case "new":
                    $cpt->new_cpt();
                    break;
                default:
                    
                    break;
            }
            
        else:
            require_once 'views/console_page_view.php';
        endif;
        
    }
    
    protected function register_shortcodes(){
        
    }
    
    protected function save_console_page(){
        update_option( 'ivycat_notes', $_POST['ivycat_notes'] );
    }
    
}

add_action( 'plugins_loaded', 'ic_IvyCat_init' );

function ic_IvyCat_init(){
    new IvyCatClass();
}
