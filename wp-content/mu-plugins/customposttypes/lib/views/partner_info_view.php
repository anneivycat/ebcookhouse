<fieldset>
    <ul>
        <li>
            <label for="partner-website">Website<label>
            <input id="partner-website" type="text" name="partner_info[website]" value="<?php echo $partner_info['website'] ?>"/>
        </li>
        <li>
            <label for="partner-company">Company<label>
            <input id="partner-company" type="text" name="partner_info[company]" value="<?php echo $partner_info['company'] ?>"/>
        </li>
        <li>
            <label for="partner-address">Address<label>
            <input id="partner-address" type="text" name="partner_info[address]" value="<?php echo $partner_info['address'] ?>"/>
        </li>
        <li>
            <label for="partner-address2">Address 2<label>
            <input id="partner-address2" type="text" name="partner_info[address2]" value="<?php echo $partner_info['address2'] ?>"/>
        </li>
        <li>
            <label for="partner-city">City<label>
            <input id="partner-city" type="text" name="partner_info[city]" value="<?php echo $partner_info['city'] ?>"/>
        </li>
        <li>
            <label for="partner-state">State<label>
            <input id="partner-state" type="text" name="partner_info[state]" value="<?php echo $partner_info['state'] ?>"/>
        </li>
        <li>
            <label for="partner-zipcode">ZipCode<label>
            <input id="partner-zipcode" type="text" name="partner_info[zipcode]" value="<?php echo $partner_info['zipcode'] ?>"/>
            <input id="partner-lattitude" type="hidden" name="partner_info[lattitude]" value="<?php echo $partner_info['lattitude'] ?>"/>
            <input id="partner-longitude" type="hidden" name="partner_info[longitude]" value="<?php echo $partner_info['longitude'] ?>"/>
        </li>
        <li>
            <label for="partner-phone">Phone<label>
            <input id="partner-phone" type="text" name="partner_info[phone]" value="<?php echo $partner_info['phone'] ?>"/>
        </li>
        <li>
            <label for="partner-fax">Fax<label>
            <input id="partner-fax" type="text" name="partner_info[fax]" value="<?php echo $partner_info['fax'] ?>"/>
        </li>
        <li>
            <label for="partner-email">Email<label>
            <input id="partner-email" type="text" name="partner_info[email]" value="<?php echo $partner_info['email'] ?>"/>
        </li>
    </ul>
</fieldset>
