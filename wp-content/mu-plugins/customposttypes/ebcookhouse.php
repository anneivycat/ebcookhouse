<?php

if (!function_exists ('add_action')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}

class EBCookHouseCPT{
    
    public function __construct(){
        add_action( 'init', array( $this, 'post_type_init' ) );
        add_action( 'admin_init' , array( &$this, 'metabox_init' ) );
        add_action( 'save_post' , array( &$this, 'save_cpt_metadata' ) );
    }
    
    public function post_type_init(){
        $labels = array(
            'name'=>__( "Menu Items" ),
            'singular_name'=>__( "Menu Item" ),
            'add_new'=>__( 'New Menu Item' ),
            'add_new_item'=>__( 'Add New Menu Item' ),
            'edit_item'=>__( 'Edit Menu Item' ),
            'new_item'=>__( 'Add New Menu Item' ),
            'view_item'=>__( 'View Menu Item' ),
            'search_items'=>__( 'Search Menu Items' )
        );
        
        // Change what you want it to support here.
        $supports = array('title','excerpt','editor','thumbnail');
        
        $args = array(
            'label'=> __( "Menu Items" ),
            'labels'=>$labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true, 
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'menu_position' => 4,
            'supports' => $supports
        );

        register_post_type( 'menu-item' ,$args);
        
        $tax = array("hierarchical" => true,
            "label" => "Menu Categories",
            "singular_label" =>"Menu Category",
            "rewrite" => array('hierarchical'=>true));
        $flag_tax = array("hierarchical" => true,
            "label" => "Nutritional Flags",
            "singular_label" =>"Nutritional Flag",
            "rewrite" => array('hierarchical'=>true));
        $type_tax = array("hierarchical" => true,
            "label" => "Menu Type",
            "singular_label" =>"Menu Type",
            "rewrite" => array('hierarchical'=>true));
        
        register_taxonomy( 'nutritional-flag', 'menu-item', $flag_tax );
        register_taxonomy( 'menu-category', 'menu-item', $tax );
        register_taxonomy( 'menu-type', 'menu-item', $type_tax );
		add_action( "menu-category_add_form_fields", array( $this, "add_form_fields" ) );
		add_action( "menu-category_edit_form_fields", array( $this, "edit_form_fields" ), 10, 2 );
		add_action( 'created_term', array( $this, 'term_type_update' ), 10, 3 );
		add_action( 'edit_term', array( $this, 'term_type_update' ), 10, 3 );
    }
    
    public function metabox_init(){
        add_meta_box(
            'Menu Iteminfo-meta',
            'Menu Item Data',
            array( &$this, 'info_metabox' ),
            'menu-item', 'normal', 'core'
        );
    }
	
	public function add_form_fields( $taxonomy ){
		echo $this->display_order_html( 0 );
	}
	
	public function edit_form_fields( $term, $taxonomy ){
		$order = ( $order_data = get_taxonomy_term_type( 'menu-category', $term ) ) ? $order_data : '0';
		echo $order_data;
		echo $this->display_order_html( $order );
	}
    
	public function term_type_update( $term_id, $tt_id, $taxonomy ) {
		if ( isset( $_POST['display-order'] ) ) {
			update_taxonomy_term_type( $taxonomy, $term_id, $_POST['display-order'] );
		}
	}
	
    public function info_metabox(){
        global $post;
        require_once 'lib/views/item_info_view.php';
    }
    
    public function save_cpt_metadata( $post_id ){
        if( defined( 'DOING_AJAX') ) return;
        if( ! is_array( $_POST['menu_item'] ) ) return;
        foreach( $_POST['menu_item'] as $item_name => $item_value ){
            update_post_meta( $post_id, 'menu_item_' . $item_name, $item_value );
        }
       
        //echo '<pre>' . print_r( $_POST, true ) . '</pre>';
    }
	
	protected function display_order_html( $order ){
		$html = '<tr class="form-field form-required">
			<th scope="row" valign="top">
				<label for="term-display-order">Display Order</label>
			</th>
			<td>
				<input id="term-display-order" type="text" name="display-order" value="' . $order . '"/>
			</td>
		</tr>';
		return $html;
	}
}

add_action( 'plugins_loaded', 'EBCookHouseCPT_init' );

function EBCookHouseCPT_init(){
    new EBCookHouseCPT();
}

function ebcook_get_menu_portion( $category ){
    $args = array(
         "post_type" => "menu-item",
         "posts_per_page" => 20,
         "tax_query" => array(
            array(
                "taxonomy" => "menu-category",
                "field" => "slug",
                "terms" => $category
            )
         )
    );
    $posts = new WP_Query( $args );
    return ( $posts->posts ) ? $posts->posts : false;
}

// helper function for retrieving and adding data for taxonomy term custom fields.
function get_taxonomy_term_type( $taxonomy, $term ) {
  return get_option( "_term_type_{$taxonomy}_{$term->term_id}" );
}
function update_taxonomy_term_type( $taxonomy, $term_id, $value) {
  update_option( "_term_type_{$taxonomy}_{$term_id}", $value );
}
