<?php

if (!function_exists ('add_action')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}

class EBCPartnersCPT{
    
    public function __construct(){
        add_action( 'init', array( $this, 'post_type_init' ) );
        add_action( 'admin_init' , array( &$this, 'metabox_init' ) );
        add_action( 'save_post' , array( &$this, 'save_cpt_metadata' ) );
    }
    
    public function post_type_init(){
        $labels = array(
            'name'=>__( "Partners" ),
            'singular_name'=>__( "Partner" ),
            'add_new'=>__( 'Add Partner' ),
            'add_new_item'=>__( 'Add New Partner' ),
            'edit_item'=>__( 'Edit Partner' ),
            'new_item'=>__( 'Add Partner' ),
            'view_item'=>__( 'View Partners' ),
            'search_items'=>__( 'Search Partners' )
        );
        
        // Change what you want it to support here.
        $supports = array('title','editor','thumbnail');
        
        $args = array(
            'label'=> __( "Partners" ),
            'labels'=>$labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true, 
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'menu_position' => 4,
            'supports' => $supports
        );

        register_post_type( 'partners' ,$args);
        
        $tax = array("hierarchical" => true,
            "label" => "Relationship",
            "singular_label" =>"Relationship",
            "rewrite" => array('hierarchical'=>true));
        $cat_tax = array("hierarchical" => true,
            "label" => "Partner Category",
            "singular_label" =>"Partner Category",
            "rewrite" => array('hierarchical'=>true));
        
        register_taxonomy( 'partner-relationship', 'partners', $tax );
        register_taxonomy( 'partner-category', 'partners', $cat_tax );
    }
    
    public function metabox_init(){
        add_meta_box(
            'Partner-meta',
            'Partner Data',
            array( &$this, 'info_metabox' ),
            'partners', 'side', 'low'
        );
    }
    
    public function info_metabox(){
        global $post;
        $partner_info = get_post_meta( $post->ID, 'ebc_partner_info', true );
        require_once 'lib/views/partner_info_view.php';
    }
    
    public function save_cpt_metadata(){
        if( defined( 'DOING_AJAX') ) return;
        if( !is_array( $_POST['partner_info'] ) ) return;
        global $post, $mappress;
        if( is_object( $mappress ) ){
            if( strlen( $_POST['partner_info']['lattitude'] ) < 1 || strlen( $_POST['partner_info']['longitude'] ) < 1 ){
                 $mp_address = new Mappress_Poi(
                    array( 
                         "address" => trim($_POST['partner_info']['address']) . " " . trim($_POST['partner_info']['city']) . ",
                         " . trim($_POST['partner_info']['state']) . ' ' . trim($_POST['partner_info']['zipcode'])
                    ) );
                    $mp_address->geocode();
                    
                    if( strlen( $mp_address->point['lat'] ) > 0 && strlen( $mp_address->point['lng'] ) > 0 ){
                        $_POST['partner_info']['lattitude'] = $mp_address->point['lat'];
                        $_POST['partner_info']['longitude'] = $mp_address->point['lng'];
                    }
            }
        }
        update_post_meta( $post->ID, 'ebc_partner_info', $_POST['partner_info'] );
        //echo '<pre>' . print_r( $_POST, true ) . '</pre>';
    }
}

add_action( 'plugins_loaded', 'EBCPartnersCPT_init' );

function EBCPartnersCPT_init(){
    new EBCPartnersCPT();
}
