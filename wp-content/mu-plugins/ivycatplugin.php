<?php
/**
 *  Plugin Name: IvyCat Plugin
 *  Description: Helper Functions, Plugin Files and necessary bits of and piecess necessary for your website.
**/

require_once 'ivycat_plugin/IvyCat.php';

/**
 *  Additional Classes and libraries
 *
 */

require_once 'customposttypes/ebcookhouse.php';
require_once 'customposttypes/vendors-and-artists.php';
