<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ebcook_cms' );

/** MySQL database username */
define( 'DB_USER', 'ebcook_WEB' );

/** MySQL database password */
define( 'DB_PASSWORD', 'C00kH0u$3!' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );


/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts. and such
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+r<v|[U]-P<#C#Vz1)4Q?(+5*MZkz`|a9A~OD{7+IWAZY[#ASE_n)Qm@Qi[zoH:(');
define('SECURE_AUTH_KEY',  'etlPO(iDyC6BfC=IzGn}KY:szYvg;nSIwqvaDK-pbr?k++QB~16p?>,m#=F_-D:S');
define('LOGGED_IN_KEY',    'f@Y{Lr6seB!ZFn3*Rvg)|]wP^bfr62opA+d7FnMVP]xw^c-)$^+]sl9=j]=qRIkn');
define('NONCE_KEY',        'ahkyx+R2TS+>(2M>m)hyyePquWtJqnFfb:U(,O`B-VM|T00[$S-[l+eqjj%m/6Jx');
define('AUTH_SALT',        'rWsvJ+^RX>0iL8!+!w-a|{Wg/}alFZ?tMa3I(qK/Z6(Iq/3ykP:CI9^5P-.mhXQ!');
define('SECURE_AUTH_SALT', 'z1~F.v/r{/9K|a%Qt4wi^R&;loF28WGK}ujIBya7+b~lECogwDpElI1:W(.lCH:!');
define('LOGGED_IN_SALT',   '~i`MRu?+@Y%gO^i&yn(NFE=]M&9SR<Z;A?vJ|+]$AI. dC^pCEh?,S-f=yTl{:[J');
define('NONCE_SALT',       'qratr$`S$Nmla~V8%x_(naUK!K.eh~AM[e5:PP9 @;O0IH.M%#.)h*vQBq;?*:aF');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'ebcook_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
